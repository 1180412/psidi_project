//imports
var express = require('express');
var bodyParser = require('body-parser');
var MediumType = require("medium-type");
var expressMongoDb = require('express-mongo-db');
var uniqid = require('uniqid');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('../swagger/catalogue_service.yaml');

//create custom Media type
var mediaTypeJsonVersion1 = new MediumType("application/vnd.fabricproduction.v1+json");

var app = express();
app.use(expressMongoDb('mongodb://localhost:27017/catalogue'));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use( bodyParser.json({ type: (req) => req.get('Content-Type') === 'application/vnd.fabricproduction.v1+json'}));
app.use( bodyParser.json({ type: (req) => req.get('Accept') === 'application/vnd.fabricproduction.v1+json'}));
app.use(bodyParser.urlencoded({extended:true}));

var now = new Date();

const port = process.env.PORT || 9001;
const SERVER_ROOT = "http://localhost:" + port;

/**
 * Functions
*/
function buildYarnObject(payload,id){
	const now = new Date();
	return {
            yarn_id : id,
            yarn_type: payload.yarn_type,
			ne: payload.ne,
            number_of_cables: payload.number_of_cables,
            number_of_filaments: payload.number_of_filaments,
            tpm: payload.tpm,
            createdOn : now,
            updatedOn: now,
		};
}
function buildColorObject(payload,id){
	const now = new Date();
	return {
            color_id : id,
            color: payload.color,
			color_category_id: payload.color_category_id,
            createdOn : now,
            updatedOn: now,
		};
}
function  buildFabricObject(payload,id){
	const now = new Date();
	return {
            fabric_id : id,
            yarns: payload.yarns,
            createdOn : now,
            updatedOn: now,
		};
}

/**
 * Yarn resource
 */
app.route("/yarn")
    .get(function(req, res) {
        //create acceptType object to parse
    let acceptType=new MediumType(req.header('accept').toString());
    //check mediaType
    if(acceptType.type==mediaTypeJsonVersion1.type
        &&acceptType.subtype==mediaTypeJsonVersion1.subtype
        &&acceptType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("yarns").find({},{"updatedOn":0,"createdOn":0,"_id":0}).toArray(function(err, result) {
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(result);
            });
    }else{
        res.status(415).json({status: "error",message: "Unsupported Media Type"});
    }
})
.post(function(req, res) {
    //create content-type object to parse
    let acceptType=new MediumType(req.header('content-type').toString());
    //check mediaType
    if(acceptType.type==mediaTypeJsonVersion1.type
        &&acceptType.subtype==mediaTypeJsonVersion1.subtype
        &&acceptType.suffix==mediaTypeJsonVersion1.suffix){
            if(req.body.ne!=='undefined'&&
            typeof req.body.number_of_cables!=='undefined'&&
            typeof req.body.number_of_filaments!=='undefined'&&
            typeof req.body.tpm!=='undefined'&&
            typeof req.body.yarn_type!=='undefined'){
                const yarnId=uniqid();
                // queue the request - handle it when possible
                setTimeout(function(){
                    //createYarnObject
                    var yarnObject =buildYarnObject(req.body,yarnId);
                    //insert object into collection
                    req.db.collection('yarns').insertOne(yarnObject);
                }, Math.random()*15000);
                res.status(202).set('Location', SERVER_ROOT + "/yarn/" + yarnId).send();
            }
            else{
                res.status(400).json({status: "error",message: "Bad Request"});
            }
    }else{
        res.status(415).json({status: "error",message: "Unsupported Media Type"});
    }
});

app.param('yarnId', function(req, res, next, yarnId){
    req.yarnId = yarnId;
    return next()
})
app.route("/yarn/:yarnId")
.get(function(req, res) {
    //create acceptType object to parse
    let contentType=new MediumType(req.header('accept').toString());
    //check mediaType
    if(contentType.type==mediaTypeJsonVersion1.type
        &&contentType.subtype==mediaTypeJsonVersion1.subtype
        &&contentType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("yarns").findOne({"yarn_id":req.yarnId},{"updatedOn":0,"createdOn":0,"_id":0},function(error, yarnObject) {
                if(yarnObject!=null){
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(yarnObject);
                }
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found yarn with id '+req.yarnId});
                }
            });
    }else{
        res.status(415).send({status: "error",message: "Unsupported Media Type"});
    }
});
/**
 * Color resource
 */
app.route("/color")
    .get(function(req,res){
        //create acceptType object to parse
        let contentType=new MediumType(req.header('accept').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
                req.db.collection("colors").find({},{"updatedOn":0,"createdOn":0,"_id":0}).toArray(function(err, result) {
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(result);
                });
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }
    })
	.post(function(req, res) {
        //create content-type object to parse
        let acceptType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(acceptType.type==mediaTypeJsonVersion1.type
            &&acceptType.subtype==mediaTypeJsonVersion1.subtype
            &&acceptType.suffix==mediaTypeJsonVersion1.suffix){
                //check payload
                if(typeof req.body.color!=='undefined'&&typeof req.body.color_category_id!=='undefined'){
                    const colorId=uniqid();
                    // queue the request - handle it when possible
                    setTimeout(function(){
                        //createColorObject
                        var colorObject =buildColorObject(req.body,colorId);
                        //insert object into collection
                        req.db.collection('colors').insertOne(colorObject);
                        delete colorObject.createdOn; 
                        delete colorObject._id; 
                    }, Math.random()*15000);
                    res.status(202).set('Location', SERVER_ROOT + "/color/" + colorId).send();
                }
                else{
                    res.status(400).type(mediaTypeJsonVersion1.toString()).send({status: "error",message: "Bad Request"});
                }
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }
	});
    app.param('colorId', function(req, res, next, colorId){
        req.colorId = colorId;
        return next()
    })
app.route("/color/:colorId")
.get(function(req, res) {
    //create acceptType object to parse
    let contentType=new MediumType(req.header('accept').toString());
    //check mediaType
    if(contentType.type==mediaTypeJsonVersion1.type
        &&contentType.subtype==mediaTypeJsonVersion1.subtype
        &&contentType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("colors").findOne({"color_id":req.colorId},{"updatedOn":0,"createdOn":0,"_id":0},function(error, colorObject) {
                if(colorObject!=null){
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(colorObject);
                }
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found color with id '+req.yarnId});
                }
            });
    
    }else{
        res.status(415).send({status: "error",message: "Unsupported Media Type"});
    }
});

/**
 * Fabric resource
*/
app.route("/fabric")
    .get(function(req,res){
        //create acceptType object to parse
        let contentType=new MediumType(req.header('accept').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
                req.db.collection("fabrics").find({},{"updatedOn":0,"createdOn":0,"_id":0}).toArray(function(err, result) {
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(result);
                });
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }
    })
	.post(function(req, res) {
        //create content-type object to parse
        let acceptType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(acceptType.type==mediaTypeJsonVersion1.type
            &&acceptType.subtype==mediaTypeJsonVersion1.subtype
            &&acceptType.suffix==mediaTypeJsonVersion1.suffix){
                //check payload
                if(typeof req.body.yarns!=='undefined'){
                    const fabricId=uniqid();
                    // queue the request - handle it when possible
                    setTimeout(function(){
                        //createFabricObject
                        var fabricObject =buildFabricObject(req.body,fabricId);
                        //insert object into collection
                        req.db.collection('fabrics').insertOne(fabricObject);
                        delete fabricObject.createdOn; 
                        delete fabricObject._id; 
                    }, Math.random()*15000);
                    res.status(202).set('Location', SERVER_ROOT + "/fabric/" + fabricId).send();
                }
                else{
                    res.status(400).type(mediaTypeJsonVersion1.toString()).send({status: "error",message: "Bad Request"});
                }
        }else{
            res.status(415).json({status: "error",message: "Unsupported Media Type"});
        }
	});
app.param('fabricId', function(req, res, next, fabricId){
    req.fabricId = fabricId;
    return next()
})
app.route("/fabric/:fabricId")
.get(function(req, res) {
    //create acceptType object to parse
    let contentType=new MediumType(req.header('accept').toString());
    //check mediaType
    if(contentType.type==mediaTypeJsonVersion1.type
        &&contentType.subtype==mediaTypeJsonVersion1.subtype
        &&contentType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("fabrics").findOne({"fabric_id":req.fabricId},{"updatedOn":0,"createdOn":0,"_id":0},function(error, fabricObject) {
                if(fabricObject!=null){
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(fabricObject);
                }
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found fabric with id '+req.fabricId});
                }
            });
    
    }else{
        res.status(415).send({status: "error",message: "Unsupported Media Type"});
    }
});

/////////////////////////////
// STARTING ...

app.listen(port, function() {
    console.log("Listening on " + port);
});