//imports
var express = require('express');
var bodyParser = require('body-parser');
var MediumType = require("medium-type");
var expressMongoDb = require('express-mongo-db');
var uniqid = require('uniqid');
var request = require('request');
const bearerToken = require('express-bearer-token');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('../swagger/production_service.yaml');
//create custom Media type
var mediaTypeJsonVersion1 = new MediumType("application/vnd.fabricproduction.v1+json");

var app = express();
app.use(expressMongoDb('mongodb://localhost:27017/sales'));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use( bodyParser.json({type: (req) => req.get('Content-Type') === 'application/vnd.fabricproduction.v1+json'}));
app.use( bodyParser.json({ type: (req) => req.get('Accept') === 'application/vnd.fabricproduction.v1+json'}));
app.use(bodyParser.urlencoded({extended: true}));
//app.use(bearerToken());
var now = new Date();
const port = process.env.PORT || 9003;
const SERVER_ROOT = "http://localhost:" + port;

/***
 * Functions
 */
function buildCustomerObject(payload,customer_id){
    const now = new Date();
    return {
        customer_id : customer_id,
        address: payload.address,
        name: payload.name,
        createdOn : now,
        updatedOn: now,
    };
}
function buildOrderObject(payload,id){
    const now = new Date();
    let orderObject= {
        order_id : id,
        fabric: payload.fabric,
        quantity_kg: payload.quantity_kg,
        customer_id: payload.customer_id,
        state: payload.state,
        total_price: payload.total_price,
        image: payload.image,
        color: payload.color,
        createdOn : now,
        updatedOn: now,
    };
	return orderObject;
}
/**
 * Request Function
 * 
 */
//This function allows to Post order in Production
function postToProductionOrder(payload,url,contentType){
    let headersOpt = {  
        "content-type": contentType,
    };
    request({
       uri : url,
       method: "POST",
       json : payload,
       headers: headersOpt
       }, 
       function(err, res, body){
            if (!err) {
                if(res.statusCode==202){
                    console.log("received");
                }
                else{
                    console.log("Posted acceptance to " + url + " and got " + res.statusCode);
                }
            }
            else {
              console.log(err);
            }
    });  
  }
/**
 * Customer resource
 */
app.route("/customer")
    .post(function(req, res) {
        //create content-type object to parse
        let acceptType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(acceptType.type==mediaTypeJsonVersion1.type
            &&acceptType.subtype==mediaTypeJsonVersion1.subtype
            &&acceptType.suffix==mediaTypeJsonVersion1.suffix){
                //check payload
                if(typeof req.body.name!=='undefined'
                &&typeof req.body.address!=='undefined'){
                    const customerId=uniqid();
                    // queue the request - handle it when possible
                    setTimeout(function(){
                        //createCustomerObject
                        var customerObject =buildCustomerObject(req.body,customerId);
                        //insert object into collection
                        req.db.collection('customers').insertOne(customerObject);
                        delete customerObject.createdOn; 
                        delete customerObject._id;
                    }, Math.random()*15000);
                    res.status(202).set('Location', SERVER_ROOT + "/customer/" + customerId).send();
                }
                else{
                    res.status(400).type(mediaTypeJsonVersion1.toString()).send({status: "error",message: "Bad Request"});
                }
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }  
});
app.param('customerId', function(req, res, next, customerId){
    req.customerId = customerId;
    return next()
})
app.route("/customer/:customerId")
.get(function(req, res) {
    //create acceptType object to parse
    let contentType=new MediumType(req.header('accept').toString());
    //check mediaType
    if(contentType.type==mediaTypeJsonVersion1.type
        &&contentType.subtype==mediaTypeJsonVersion1.subtype
        &&contentType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("customers").findOne({"customer_id":req.customerId},{"updatedOn":0,"createdOn":0,"_id":0},function(error, customerObject) {
                if(customerObject!=null){
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(customerObject);
                }
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found customer with id '+req.customerId});
                }
            });
    }else{
        res.status(415).send({status: "error",message: "Unsupported Media Type"});
    }
});
/**
 * Order resource
 */
app.route("/order")
.get(function(req,res){
    let contentType=new MediumType(req.header('content-type').toString());
    //check mediaType
    if(contentType.type==mediaTypeJsonVersion1.type
        &&contentType.subtype==mediaTypeJsonVersion1.subtype
        &&contentType.suffix==mediaTypeJsonVersion1.suffix){
            console.log(req.query);
            if( typeof req.query.customer_id !=='undefined'
                && typeof req.query.state !=='undefined'){
                req.db.collection("orders").find({"customer_id":req.query.customer_id,"status":req.query.status},{"updatedOn":0,"createdOn":0,"_id":0}).toArray(function(error, orderObject) {
                    if(orderObject!=null){
                        res.status(200).type(mediaTypeJsonVersion1.toString()).send(orderObject);
                    }
                    else{
                        res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found order with customer_id '+req.query.customer_id});
                    }
                });    
            }
            else{
                res.status(400).type(mediaTypeJsonVersion1.toString()).send({status: "error",message: "Bad Request"});
            }
        }
        else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }
})
.post(function(req,res){
    let contentType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
                //check payload
                if(typeof req.body.customer_id!=='undefined'
                &&typeof req.body.quantity_kg!=='undefined'
                &&typeof req.body.fabric!=='undefined'){
                    const orderId=uniqid();
                    // queue the request - handle it when possible
                    setTimeout(function(){
                        //Get Json Object
                        let payload=req.body;
                        if(typeof payload.color!=='undefined')
                            payload.color=null;
                        if(typeof payload.image!=='undefined')
                            payload.image=null;
                        //Init order State
                        payload.state="Processing";
                        //Init order Price
                        payload.total_price="Processing";
                        //createOrderObject
                        var orderObject =buildOrderObject(payload,orderId);
                        //insert object into collection
                        req.db.collection('orders').insertOne(orderObject);
                        //Create object for Post To Production Order
                        let productionOrderObject=orderObject;
                        delete productionOrderObject.customer_id;
                        delete productionOrderObject.createdOn; 
                        delete productionOrderObject._id;
                        delete productionOrderObject.order_id;
                        //Post To Production Order
                        productionOrderObject.callback=SERVER_ROOT+"/order/"+orderId;
                        postToProductionOrder(productionOrderObject,"http://localhost:9004/order",contentType);
                    }, Math.random()*15000);
                    res.status(202).set('Location', SERVER_ROOT + "/order/" + orderId).send();
                }
                else{
                    res.status(400).type(mediaTypeJsonVersion1.toString()).send({status: "error",message: "Bad Request"});
                }
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        } 
});
app.param('orderId', function(req, res, next, orderId){
    req.orderId = orderId;
    return next()
})
app.route("/order/:orderId")
.get(function(req, res) {
    //create acceptType object to parse
    let acceptType=new MediumType(req.header('accept').toString());
    //check mediaType
    if(acceptType.type==mediaTypeJsonVersion1.type
        &&acceptType.subtype==mediaTypeJsonVersion1.subtype
        &&acceptType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("orders").findOne({"order_id":req.orderId},{"updatedOn":0,"createdOn":0,"_id":0},function(error, orderObject) {
                if(orderObject!=null){
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(orderObject);
                }
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found order with id '+req.orderId});
                }
            });
    }else{
        res.status(415).send({status: "error",message: "Unsupported Media Type"});
    }
})
.put(function(req,res){
    let contentType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("orders").findOne({"order_id":req.orderId},{},function(error, orderObject) {
                if(orderObject!=null){
                    if(typeof req.body.state!=='undefined')
                        orderObject.state=req.body.state;
                    if(typeof req.body.total_price!=='undefined')
                        orderObject.total_price=req.body.total_price;
                    let now = new Date();
                    orderObject.updatedOn=now;
                    req.db.collection("orders").updateOne({"order_id": req.orderId}, orderObject, function(err, orderObjectUpdated) {
                        if(orderObjectUpdated!=null){
                            delete orderObjectUpdated.createdOn; 
                            delete orderObjectUpdated._id;
                            delete orderObjectUpdated.updatedOn;
                            res.status(200).type(mediaTypeJsonVersion1.toString()).send(orderObjectUpdated);
                        }
                    });
                }
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found order with id '+req.orderId});
                }
        });
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        } 
});
/////////////////////////////
// STARTING ...
app.listen(port, function() {
    console.log("Listening on " + port);
});