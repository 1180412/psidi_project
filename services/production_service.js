//imports
var express = require('express');
var bodyParser = require('body-parser');
var MediumType = require("medium-type");
var expressMongoDb = require('express-mongo-db');
var uniqid = require('uniqid');
const bearerToken = require('express-bearer-token');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var request = require('request');
var amqp = require('amqplib/callback_api');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('../swagger/production_service.yaml');
//create custom Media type
var mediaTypeJsonVersion1 = new MediumType("application/vnd.fabricproduction.v1+json");

var app = express();
app.use(expressMongoDb('mongodb://localhost:27017/production'));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use( bodyParser.json({type: (req) => req.get('Content-Type') === 'application/vnd.fabricproduction.v1+json'}));
app.use( bodyParser.json({ type: (req) => req.get('Accept') === 'application/vnd.fabricproduction.v1+json'}));
app.use(bodyParser.urlencoded({extended: true}));
//app.use(bearerToken());
var now = new Date();
const port = process.env.PORT || 9004;
const SERVER_ROOT = "http://localhost:" + port;
const EXCHANGE_NAME="suppliers";
const userId="fabricProduction";

/**
* Functions
*/
function buildSupplierObject(payload,id,bindingKey,index_bindingKey,exchangeName){
    const now = new Date();
    let supplierObject= {
        supplier_id : id,
        binding_key_index:index_bindingKey,
        name: payload.name,
        date_of_price_offer : payload.date_of_price_offer,
        offer_validity : payload.offer_validity,
        yarns_available : payload.yarns_available,
        binding_key : bindingKey,
        exchange_name: exchangeName,
        createdOn : now,
        updatedOn: now,
    };
    return supplierObject;
}
function buildOrderObject(payload,id){
    const now = new Date();
    let orderObject= {
        order_id : id,
        fabric: payload.fabric,
        image: payload.image,
        color: payload.color,
        supplier: payload.supplier,
        delivery_date: payload.delivery_date,
        quantity_kg: payload.quantity_kg,
        state: payload.state,
        total_price: payload.total_price,
        image: payload.image,
        color: payload.color,
        createdOn : now,
        updatedOn: now,
    };
	return orderObject;
}

function buildProcessObject(payload,id){
    const now = new Date();
    let processObject= {
        process_id : id,
        sequence: payload.sequence,
        process_name: payload.process_name,
        price_per_kg: payload.price_per_kg,
        createdOn : now,
        updatedOn: now,
    };
	return processObject;
}
function checkYarnsAvailable(supplierYarnsAvailable,requestedYarns){
    if(supplierYarnsAvailable.length==0) return false;
    for (let j = 0; j < requestedYarns.length; j++) {
        for (let i = 0; i < supplierYarnsAvailable.length; i++) {
            if(requestedYarns[j].yarn_type==supplierYarnsAvailable[i].yarn_type
                &&requestedYarns[j].ne==supplierYarnsAvailable[i].ne
                &&requestedYarns[j].number_of_filaments==supplierYarnsAvailable[i].number_of_filaments
                &&requestedYarns[j].tpm==supplierYarnsAvailable[i].tpm) break;
           
            if((supplierYarnsAvailable.length-1)==i) return false; 
        }
    }
    return true;
}
function checkCurrentDateIsinBetween(date1,date2){
    let date1Aux = Date.parse(date1);
    let date2Aux = Date.parse(date2);
    let today = new Date();
    if((today <= date2Aux && today >= date1Aux)) 
        return true;

    return false;
}
function getSupplierByName(suppliersRoutingsKeys,supplierUserId){
        for(let i=0;i<suppliersRoutingsKeys.length;i++) {
           if(suppliersRoutingsKeys[i].name==supplierUserId) return suppliersRoutingsKeys[i];
        }
}
function calcTotalPrice(supplierYarnsAvailable,requestedYarns){
    let totalPrice=0;
    console.log(supplierYarnsAvailable);
    console.log(requestedYarns);
    for (let j = 0; j < requestedYarns.length; j++) {
        for (let i = 0; i < supplierYarnsAvailable.length; i++) {
            if(requestedYarns[j].yarn_type==supplierYarnsAvailable[i].yarn_type
                &&requestedYarns[j].ne==supplierYarnsAvailable[i].ne
                &&requestedYarns[j].number_of_filaments==supplierYarnsAvailable[i].number_of_filaments
                &&requestedYarns[j].tpm==supplierYarnsAvailable[i].tpm){
                    totalPrice+=supplierYarnsAvailable[i].price_per_kg*requestedYarns[j].quantity_kg;
                    break;
                }
        }
    }
    return totalPrice;
}
function processingOrder(req,orderId,contentType){
    let yarnsArrayInKg=convertPercentageToQuantityKgYarns(req.body.quantity_kg,req.body.fabric);
    req.db.collection("suppliers").find().toArray(function(error,result){
        if(result!=null){
            var suppliersRoutingsKeys=[];
            result.forEach(function(supplier) {
                if(checkCurrentDateIsinBetween(supplier.date_of_price_offer,supplier.offer_validity)
                &&checkYarnsAvailable(supplier.yarns_available,yarnsArrayInKg)){
                    suppliersRoutingsKeys.push(supplier);
                } 
            });
            var routingKey = buildRoutingKey(suppliersRoutingsKeys); 
            var options = {replyTo:"suppliers_reply",correlationId: orderId,userId: userId};
            var jsonRpc={jsonrpc:"2.0",id: orderId, method:"order", params:yarnsArrayInKg};
            var supplierSelected=null;
            let supplierSelectedObject;
            let totalPrice=0;
            amqp.connect('amqp://fabricProduction:fabricProduction@localhost', function(err, conn) {
                conn.createChannel(function(err, ch) {
                    ch.assertExchange(EXCHANGE_NAME, 'topic', {durable: false});
                        ch.assertQueue('', {exclusive: true}, function(err, q) {
                            options.replyTo=q.queue.toString();
                            console.log("Send to all Suppliers that have those yarns Requested by Customer - (Queue - via Topic)");
                            ch.publish(EXCHANGE_NAME, routingKey,Buffer.from(JSON.stringify(jsonRpc)),options);
                            ch.consume(q.queue, function(msg) {
                                console.log("Received ack from Suppliers");
                                if (msg.properties.correlationId === orderId) {
                                    let jsonRpc=JSON.parse(msg.content.toString('utf8'));
                                    if(typeof jsonRpc.result!=='undefined'
                                    &&typeof jsonRpc.result.delivery_date!=='undefined'){
                                        totalPrice=calcTotalPrice(supplierSelectedObject.yarns_available,yarnsArrayInKg);
                                        finishOrder(jsonRpc.result.order_id,totalPrice,jsonRpc.result.delivery_date,supplierSelectedObject,req.body.callback,req.db,contentType);
                                    }
                                    //selected supplier and ignore others
                                    if(supplierSelected==null&&typeof jsonRpc.result!=='undefined')
                                        supplierUserIdSelected=msg.properties.userId;

                                    if(typeof jsonRpc.result!=='undefined'&&jsonRpc.result.method=="confirmation"
                                    &&supplierUserIdSelected==msg.properties.userId){
                                        let jsonRpcConfirmation={jsonrpc:"2.0",id: orderId, method:jsonRpc.result.method, params:jsonRpc.result.order_id};
                                        supplierSelectedObject=getSupplierByName(suppliersRoutingsKeys,supplierUserIdSelected);
                                        console.log("Send to RoutingKey: "+supplierSelectedObject.binding_key+" - (Queue - via Topic)");
                                        ch.publish(EXCHANGE_NAME, supplierSelectedObject.binding_key,Buffer.from(JSON.stringify(jsonRpcConfirmation)),options);
                                    }
                                }
                            }, {noAck: true});
                        });
                });
            });

        }
    });
    
}

//colocar data na order
//calcular o preço total com os processos
//fazer PUT ao Sale do preço final
//
function finishOrder(orderId,totalPrice,deliveryDate,supplier,callback,db,contentType){
    db.collection("orders").updateOne({"order_id":orderId},{"$set":{"total_price":totalPrice,"delivery_date":deliveryDate,"supplier":supplier}}, function(err, orderObjectUpdated) {
        if(orderObjectUpdated!=null){
            let orderPayload={
                                "total_price": totalPrice,
                                "delivery_date": deliveryDate,
                            };
            productionProcessing(callback,orderPayload,contentType);
        }
    });

}
/**
 * This functions allows
 * to sort array by key 
 */
function sortByKey(array, key) {
    return array.sort(function(a, b) {
        let x = a[key]; let y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}
/**
 * 
 * This function allows to
 * generate RoutingKey
 */
function buildRoutingKey(suppliersArray){
    let routingKey="";
    let sortedSuppliersArray=sortByKey(suppliersArray,"binding_key_index");
    for(let i=0;i<sortedSuppliersArray.length;i++){
        if(i==sortedSuppliersArray[i].binding_key_index)
            routingKey+=sortedSuppliersArray[i].name+".";
        else
            routingKey+="*.";
    }
    routingKey+="#";
    return routingKey;
}

/* 
/* This function allows to converts percentage in kg of yarns
/* This resource receive quantity in kg of fabric and percentage of yarns, 
/* to perform an order to Suppliers, the service need to convert in
/* percentage in kg of specific yarns.
*/
function convertPercentageToQuantityKgYarns(quantity_kg,fabric){
    let arrayOfYarnsInPercentage=fabric.yarns;
    let arrayOfYarnsInKg=[];
    arrayOfYarnsInPercentage.forEach(function(yarn) {
        let yarnInKg= Object.assign({}, yarn);
        yarnInKg.quantity_kg=(yarn.percentage/100)*quantity_kg;
        delete yarnInKg.percentage;
        delete yarnInKg.yarn_id;
        arrayOfYarnsInKg.push(yarnInKg);
      });
      return arrayOfYarnsInKg;
}
/**
 * Production Processing function
 * 
 */
function productionProcessing(urlCallback,orderObject,contentType){
    let arrayProcesses=["Knitting","Pretreatment","Finishing","Inspection","Packaging"];
    let headersOpt = {  
        "content-type": contentType,
    };
    for(let i=0;i<arrayProcesses.length;i++){
        let process=arrayProcesses[i].toString();
        // queue the request - handle it when possible
        setTimeout(function(){
            orderObject.state=process;
            request({
                uri : urlCallback,
                method: "PUT",
                json : orderObject,
                headers: headersOpt
                }, 
                function(err, res, body){
                     if (!err) {
                         if(res.statusCode==200)
                             console.log('Updated'+ orderObject.state);
                         else
                             console.log("Posted acceptance to " + urlCallback + " and got " + res.statusCode);
                     }
                     else 
                       console.log(err);
             });  
        }, i*15000);
    }
}

/**
 * Process resource
 * 
 */
app.route("/process")
    .post(function(req, res) {
        //create content-type object to parse
        let contentType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
                //check payload
                if(typeof req.body.process_name!=='undefined'
                &&typeof req.body.sequence!=='undefined'
                &&typeof req.body.price_per_kg!=='undefined'){
                    const processId=uniqid();
                    // queue the request - handle it when possible
                    setTimeout(function(){
                        //createProcessObject
                        let processObject =buildProcessObject(req.body,processId);
                        //insert object into collection
                        req.db.collection('processes').insertOne(processObject);
                    }, Math.random()*15000);
                    res.status(202).set('Location', SERVER_ROOT + "/process/" + processId).send();
                }
                else{
                    res.status(400).type(mediaTypeJsonVersion1.toString()).send({status: "error",message: "Bad Request"});
                }
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }  
});
app.param('processId', function(req, res, next, processId){
    req.processId = processId;
    return next()
})
app.route("/process/:processId")
.get(function(req, res) {
    //create acceptType object to parse
    let acceptType=new MediumType(req.header('accept').toString());
    //check mediaType
    if(acceptType.type==mediaTypeJsonVersion1.type
        &&acceptType.subtype==mediaTypeJsonVersion1.subtype
        &&acceptType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("processes").findOne({"process_id":req.processId},{"updatedOn":0,"createdOn":0,"_id":0},function(error, processObject) {
                if(processObject!=null)
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(processObject);
                else
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found process with id '+req.processId});
            });
    }else{
        res.status(415).send({status: "error",message: "Unsupported Media Type"});
    }
})
.put(function(req,res){
    let contentType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("processes").findOne({"process_id":req.processId},{},function(error, processObject) {
                if(processObject!=null){
                    if(typeof req.body.sequence!=='undefined')
                        processObject.sequence=req.body.sequence;
                    if(typeof req.body.price_per_kg!=='undefined')
                        processObject.price_per_kg=req.body.price_per_kg;
                    let now = new Date();
                    processObject.updatedOn=now;
                    req.db.collection("processes").updateOne({"process_id": req.processId}, processObject, function(err, processObjectUpdated) {
                        if(processObjectUpdated!=null){
                            delete processObjectUpdated.createdOn; 
                            delete processObjectUpdated._id;
                            res.status(200).type(mediaTypeJsonVersion1.toString()).send(processObjectUpdated);
                        }
                    });
                }
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found process with id '+req.processId});
                }
        });
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        } 
});
/**
 * Color Category resource
 * 
 */
app.route("/colorcategory")
    .post(function(req, res) {
        //create content-type object to parse
        let contentType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
                //check payload
                if(typeof req.body.category_name!=='undefined'
                &&typeof req.body.price_per_kg!=='undefined'){
                    const colorcategoryId=uniqid();
                    // queue the request - handle it when possible
                    setTimeout(function(){
                        //createColorCategoryObject
                        let colorCategoryObject =buildColorCategoryObject(req.body,colorcategoryId);
                        //insert object into collection
                        req.db.collection('colorcategories').insertOne(colorCategoryObject);
                    }, Math.random()*15000);
                    res.status(202).set('Location', SERVER_ROOT + "/colorcategory/" + colorcategoryId).send();
                }
                else{
                    res.status(400).type(mediaTypeJsonVersion1.toString()).send({status: "error",message: "Bad Request"});
                }
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }  
});
app.param('colorcategoryId', function(req, res, next, colorcategoryId){
    req.colorcategoryId = colorcategoryId;
    return next()
})
app.route("/colorcategory/:colorcategoryId")
.get(function(req, res) {
    //create acceptType object to parse
    let acceptType=new MediumType(req.header('accept').toString());
    //check mediaType
    if(acceptType.type==mediaTypeJsonVersion1.type
        &&acceptType.subtype==mediaTypeJsonVersion1.subtype
        &&acceptType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("colorcategories").findOne({"colorcategory_id":req.colorcategoryId},{"updatedOn":0,"createdOn":0,"_id":0},function(error, colorcategoryObject) {
                if(colorcategoryObject!=null){
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(colorcategoryObject);
                }
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found color category with id '+req.colorcategoryId});
                }
            });
    }else{
        res.status(415).send({status: "error",message: "Unsupported Media Type"});
    }
})
.put(function(req,res){
    let contentType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("colorcategories").findOne({"colorcategory_id":req.colorcategoryId},{},function(error, colorcategoryObject) {
                if(colorcategoryObject!=null){
                    if(typeof req.body.price_per_kg!=='undefined')
                    colorcategoryObject.price_per_kg=req.body.price_per_kg;
                    let now = new Date();
                    colorcategoryObject.updatedOn=now;
                    req.db.collection("colorcategories").updateOne({"colorcategory_id": req.colorcategoryId}, colorcategoryObject, function(err, colorcategoryObjectUpdated) {
                        if(colorcategoryObjectUpdated!=null){
                            delete colorcategoryObjectUpdated.updatedOn; 
                            delete colorcategoryObjectUpdated.createdOn; 
                            delete colorcategoryObjectUpdated._id;
                            res.status(200).type(mediaTypeJsonVersion1.toString()).send(colorcategoryObjectUpdated);
                        }
                    });
                }
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found color category with id '+req.colorcategoryId});
                }
        });
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        } 
});

/**
 * Order resource
 * 
 */
app.route("/order")
    .post(function(req, res) {
        //create content-type object to parse
        let contentType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
                //check payload
                if(typeof req.body.fabric!=='undefined'
                &&typeof req.body.quantity_kg!=='undefined'){
                    const orderId=uniqid();
                    // queue the request - handle it when possible
                    setTimeout(function(){
                        //createOrderObject
                        req.body.state="Processing";
                        req.body.total_price="Processing";
                        req.body.delivery_date="Processing";
                        req.body.supplier="Processing";
                        let orderObject =buildOrderObject(req.body,orderId);
                        //insert object into collection
                        req.db.collection('orders').insertOne(orderObject,function(err,orderObject){
                            processingOrder(req,orderId,contentType);
                        });

                    }, Math.random()*15000);
                    res.status(202).set('Location', SERVER_ROOT + "/order/" + orderId).send();
                }
                else{
                    res.status(400).type(mediaTypeJsonVersion1.toString()).send({status: "error",message: "Bad Request"});
                }
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }  
});


/**
 * Supplier resource
 * 
 */
app.route("/supplier")
    .post(function(req, res) {
        //create content-type object to parse
        let contentType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
                //check payload
                if(typeof req.body.name!=='undefined'
                &&typeof req.body.yarns_available!=='undefined'
                &&typeof req.body.offer_validity!=='undefined'
                &&typeof req.body.date_of_price_offer!=='undefined'){
                    const supplierId=uniqid();
                    // queue the request - handle it when possible
                    setTimeout(function(){
                        req.db.collection("suppliers").count(function(err,numberOfKeys){
                            let bindingKey="";
                            let indexBindingKey;
                            for(indexBindingKey=0;indexBindingKey<numberOfKeys;indexBindingKey++)
                                bindingKey+="*.";
                        
                            bindingKey+=req.body.name+".#";

                            //createSupplierObject
                            let supplierObject =buildSupplierObject(req.body,supplierId,bindingKey,indexBindingKey,EXCHANGE_NAME);
                            //insert object into collection
                            req.db.collection('suppliers').insertOne(supplierObject);
                        });    
                    }, Math.random()*15000);
                    res.status(202).set('Location', SERVER_ROOT + "/supplier/" + supplierId).send();
                }
                else{
                    res.status(400).type(mediaTypeJsonVersion1.toString()).send({status: "error",message: "Bad Request"});
                }
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }  
});
app.param('supplierId', function(req, res, next, supplierId){
    req.supplierId = supplierId;
    return next()
})
app.route("/supplier/:supplierId")
.get(function(req, res) {
    //create acceptType object to parse
    let acceptType=new MediumType(req.header('accept').toString());
    //check mediaType
    if(acceptType.type==mediaTypeJsonVersion1.type
        &&acceptType.subtype==mediaTypeJsonVersion1.subtype
        &&acceptType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("suppliers").findOne({"supplier_id":req.supplierId},{"updatedOn":0,"createdOn":0,"_id":0},function(error, supplierObject) {
                if(supplierObject!=null){
                    res.status(200).type(mediaTypeJsonVersion1.toString()).send(supplierObject);
                }
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found supplier with id '+req.supplierId});
                }
            });
    }else{
        res.status(415).send({status: "error",message: "Unsupported Media Type"});
    }
})
.put(function(req,res){
    let contentType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
            req.db.collection("suppliers").findOne({"supplier_id":req.supplierId},{},function(error, supplierObject) {
                if(supplierObject!=null){
                        if(typeof req.body.date_of_price_offer!=='undefined'
                        &&typeof req.body.offer_validity!=='undefined'
                        &&typeof req.body.yarns_available!=='undefined'){
                        
                        supplierObject.date_of_price_offer=req.body.date_of_price_offer;
                        supplierObject.offer_validity=req.body.offer_validity;
                        supplierObject.yarns_available=req.body.yarns_available;
                        let now = new Date();
                        supplierObject.updatedOn=now;
                        req.db.collection("suppliers").updateOne({"supplier_id": req.supplierId}, supplierObject, function(err, supplierObjectUpdated) {
                            if(supplierObjectUpdated!=null){
                                delete supplierObjectUpdated.updatedOn; 
                                delete supplierObjectUpdated.createdOn; 
                                delete supplierObjectUpdated._id;
                                res.status(200).type(mediaTypeJsonVersion1.toString()).send(supplierObjectUpdated);
                            }
                        });
                    }else{
                        res.status(400).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Bad Request'});
                    }
                }   
                else{
                    res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found color category with id '+req.colorcategoryId});
                }
        });
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        } 
});


/////////////////////////////
// STARTING ...

app.listen(port, function() {
    console.log("Listening on " + port);
});