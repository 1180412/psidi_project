//imports
var express = require('express');
var bodyParser = require('body-parser');
var MediumType = require("medium-type");
var expressMongoDb = require('express-mongo-db');
var uniqid = require('uniqid');

//create custom Media type
var mediaTypeJsonVersion1 = new MediumType("application/vnd.fabricproduction.v1+json");

var app = express();
app.use(expressMongoDb('mongodb://localhost:27017/invoices'));
app.use( bodyParser.json({ type: (req) => req.get('Content-Type') === 'application/vnd.fabricproduction.v1+json'}));
app.use( bodyParser.json({ type: (req) => req.get('Accept') === 'application/vnd.fabricproduction.v1+json'}));
app.use(bodyParser.urlencoded({extended:true}));

var now = new Date();

const port = process.env.PORT || 9002;
const SERVER_ROOT = "http://localhost:" + port;

/**
 * Functions
 */
function buildInvoiceObject(payload,invoiceId){
	const now = new Date();
	return {
            invoiceId : invoiceId,
            customer_id: payload.customer_id,
			total_cost: payload.total_cost,
            invoice_number: payload.invoice_number,
            date:payload.date,
            createdOn : now,
            updatedOn: now,
		};
}

/**
 * Invoice resource
 */
app.route("/invoice")
	.post(function(req, res) {
        //create content-type object to parse
        let acceptType=new MediumType(req.header('content-type').toString());
        //check mediaType
        if(acceptType.type==mediaTypeJsonVersion1.type
            &&acceptType.subtype==mediaTypeJsonVersion1.subtype
            &&acceptType.suffix==mediaTypeJsonVersion1.suffix){
                if(req.body.date!=='undefined'&&
                typeof req.body.customer_id!=='undefined'&&
                typeof req.body.total_cost!=='undefined'&&
                typeof req.body.invoice_number!=='undefined'){
                    const invoiceId=uniqid();
                    setTimeout(function(){
                        //createYarnObject
                        var invoiceObject =buildInvoiceObject(req.body,invoiceId);
                        //insert object into collection
                        req.db.collection('invoices').insertOne(invoiceObject);
                        delete invoiceObject.createdOn; 
                        delete invoiceObject._id; 
                    }, Math.random()*15000);
                    res.status(202).set('Location', SERVER_ROOT + "/invoice/" + invoiceId).send();
                }
                else{
                    res.status(400).type(mediaTypeJsonVersion1.toString()).send({status: "error",message: "Bad Request"});
                }
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }
    });
    app.param('invoiceId', function(req, res, next, invoiceId){
        req.invoiceId = invoiceId;
        return next()
    })
    app.route("/invoice/:invoiceId")
    .get(function(req, res) {
        //create acceptType object to parse
        let contentType=new MediumType(req.header('accept').toString());
        //check mediaType
        if(contentType.type==mediaTypeJsonVersion1.type
            &&contentType.subtype==mediaTypeJsonVersion1.subtype
            &&contentType.suffix==mediaTypeJsonVersion1.suffix){
                req.db.collection("invoices").findOne({"invoiceId":req.invoiceId},{"updatedOn":0,"createdOn":0,"_id":0},function(error, invoiceObject) {
                    if(invoiceObject!=null){
                        res.status(200).type(mediaTypeJsonVersion1.toString()).send(invoiceObject);
                    }
                    else{
                        res.status(404).type(mediaTypeJsonVersion1.toString()).send({status:'Error',message:'Not found invoice with id '+req.invoiceId});
                    }
                });
        
        }else{
            res.status(415).send({status: "error",message: "Unsupported Media Type"});
        }
    });

/////////////////////////////
// STARTING ...

app.listen(port, function() {
    console.log("Listening on " + port);
});