var amqp = require('amqplib/callback_api');
var MongoClient = require('mongodb');
var url = "mongodb://localhost:27017";
var now = new Date();
var uniqid = require('uniqid');
const userId="supplier1";
const DATA_BASE_NAME="supplier1";
amqp.connect('amqp://supplier1:supplier1@localhost', function(err, conn) {
    if(err)
    console.log(err);
  conn.createChannel(function(err, ch) {
    var ex = 'suppliers';
    ch.assertExchange(ex, 'topic', {durable: false});
    ch.assertQueue('', {exclusive: true}, function(err, q) {
        ch.bindQueue(q.queue, ex, "*supplier1.#");
            ch.consume(q.queue, function(msg) {
                //queue the request - handle it when possible
                setTimeout(function(){
                    console.log("Received Message");
                    let jsonRpc=JSON.parse(msg.content.toString('utf8'));
                    if(typeof jsonRpc.method=='undefined'
                    &&typeof jsonRpc.params=='undefined'){
                        let jsonRpcResponse={
                                                jsonrpc: "2.0",
                                                error: {
                                                            code: -32600,
                                                            message: "Invalid Request",
                                                            data: "Doesn't exist method or params"
                                                        },
                                                id: jsonRpc.id
                                            };
                        ch.sendToQueue(msg.properties.replyTo.toString(),Buffer.from(JSON.stringify(jsonRpcResponse)),{correlationId: msg.properties.correlationId,userId:userId});
                    }
                    switch (jsonRpc.method){
                        case "order":
                            console.log("Processing Order");
                            order(jsonRpc.params,ch,jsonRpc,msg);
                            break;
                        case "confirmation":
                            console.log("Processing Confirmation");
                            confirmation(jsonRpc.params,ch,jsonRpc,msg);
                            break;
                        default:
                            let jsonRpcResponse={
                                jsonrpc: "2.0",
                                error: {
                                            code: -32602,
                                            message: "Invalid params",
                                            data: "Invalid method parameter(s)."
                                        },
                                id: jsonRpc.id
                            };
                            ch.sendToQueue(msg.properties.replyTo.toString(),Buffer.from(JSON.stringify(jsonRpcResponse)),{correlationId: msg.properties.correlationId,userId:userId});
                            break;   
                    }
                }, Math.random()*15000);
            }, {noAck: true});
    });
  });
});

function buildOrderObject(id,yarnsArray,status){
    const now = new Date();
    let expiresOn = new Date(new Date().getTime()+(5*60*1000));
    let orderObject= {
        order_id : id,
        yarns : yarnsArray,
        status: status,
        expiresOn: expiresOn,
        acceptedOn : now,
        updatedOn: now,
    };
	return orderObject;
}
function insertOrderDb(clientYarnsArray,ch,jsonRpc,msg){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
        if (err) throw err;
        const orderId=uniqid();
        let orderObject=buildOrderObject(orderId,clientYarnsArray,"Wait for confirmation");
        var db = client.db(DATA_BASE_NAME);
        db.collection("orders").insertOne(orderObject, function(err, orderObjectInserted) {
            if (err) throw err;
            if(orderObjectInserted!=null){
                let jsonRpcResponse={
                                        jsonrpc: "2.0",
                                        result: { 
                                            order_id: orderId,
                                            method: "confirmation"
                                        },
                                        id: jsonRpc.id
                                    };
                ch.sendToQueue(msg.properties.replyTo.toString(),Buffer.from(JSON.stringify(jsonRpcResponse)),{correlationId: msg.properties.correlationId,userId:userId});
            }
        });
    });
}

function buildAndCompareArrayOfYarns(clientYarnsArray,stockYarnsArray){
    let ownArrayOfYarnsRequested=[];
    if(stockYarnsArray.length==0) return ownArrayOfYarnsRequested;
    for (let j = 0; j < clientYarnsArray.length; j++) {
        for (let i = 0; i < stockYarnsArray.length; i++) {
            if(clientYarnsArray[j].yarn_type==stockYarnsArray[i].yarn_type
                &&clientYarnsArray[j].ne==stockYarnsArray[i].ne
                &&clientYarnsArray[j].number_of_filaments==stockYarnsArray[i].number_of_filaments
                &&clientYarnsArray[j].tpm==stockYarnsArray[i].tpm
                &&clientYarnsArray[j].quantity_kg<=stockYarnsArray[i].quantity_kg){
                    stockYarnsArray[i].quantity_kg=clientYarnsArray[j].quantity_kg;
                    ownArrayOfYarnsRequested.push(stockYarnsArray[i]);
                    break;
                }
        }
    }
    return ownArrayOfYarnsRequested;
}
function confirmationStock(clientYarnsArray,ch,jsonRpc,msg){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
        if (err) throw err;
        var db = client.db(DATA_BASE_NAME);
        db.collection("yarns").find().toArray(function(err, stockYarnsArray) {
            if (err) throw err;
            let ownArrayOfYarnsRequested=[];
            ownArrayOfYarnsRequested=buildAndCompareArrayOfYarns(clientYarnsArray,stockYarnsArray);
            if(ownArrayOfYarnsRequested.length==clientYarnsArray.length)
                insertOrderDb(ownArrayOfYarnsRequested,ch,jsonRpc,msg);
            else{
                let jsonRpcResponse={
                    jsonrpc: "2.0",
                    error: {
                             code: -32602,
                             message: "Invalid params",
                             data: "Not available in this moment"
                          },
                    id: jsonRpc.id
                };
                ch.sendToQueue(msg.properties.replyTo.toString(),Buffer.from(JSON.stringify(jsonRpcResponse)),{correlationId: msg.properties.correlationId,userId:userId});
            }
        });
    });
}
function order(clientYarnsArray,ch,jsonRpc,msg){
    confirmationStock(clientYarnsArray,ch,jsonRpc,msg);
}

function createArrayUpdateYarn(arrayYarns,stockYarnsArray){
    let arrayOfYarnsRequested=[];
    if(stockYarnsArray.length==0) return arrayOfYarnsRequested;
    for (let j = 0; j < arrayYarns.length; j++) {
        for (let i = 0; i < stockYarnsArray.length; i++) {
            if(arrayYarns[j].yarn_type==stockYarnsArray[i].yarn_type
                &&arrayYarns[j].ne==stockYarnsArray[i].ne
                &&arrayYarns[j].number_of_filaments==stockYarnsArray[i].number_of_filaments
                &&arrayYarns[j].tpm==stockYarnsArray[i].tpm
                &&arrayYarns[j].quantity_kg<=stockYarnsArray[i].quantity_kg){
                    stockYarnsArray[i].quantity_kg=stockYarnsArray[i].quantity_kg-arrayYarns[j].quantity_kg;
                    arrayOfYarnsRequested.push(stockYarnsArray[i]);
                    break;
                }
        }
    }
    return arrayOfYarnsRequested;
}
function updateStock(arrayYarns){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
        if (err) throw err;
        var db = client.db(DATA_BASE_NAME);
        arrayYarns.forEach(yarn => {
            db.collection("yarns").find().toArray(function(err, yarnsArray) {
                if (err) throw err;
                arrayYarnsToUpdate=createArrayUpdateYarn(arrayYarns,yarnsArray);
                arrayYarnsToUpdate.forEach(yarn => {
                    db.collection("yarns").replaceOne({"_id": yarn._id}, yarn, function(err, yarnObjectUpdated) {
                        if (err) throw err;
                    });
                });
            });
        });
    });
}

function processingConfirmationOrder(orderId,ch,jsonRpc,msg){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
        if (err) throw err;
        var db = client.db(DATA_BASE_NAME);
        db.collection("orders").findOne({"order_id":orderId},{},function(err, orderObject) {
            if (err) throw err;
            let expiresOn=Date(orderObject.expiresOn);
            let now = new Date();
            if(orderObject!=null){
                orderObject.status="Confirmed";
                db.collection("orders").replaceOne({"order_id": orderId},orderObject, function(err, orderObjectUpdated) {
                    if (err) throw err;
                    if(orderObjectUpdated){
                        let delivery_date= new Date(new Date().getTime()+(5*24*60*60*1000));
                        let jsonRpcResponse={
                            jsonrpc: "2.0",
                            result: {
                                delivery_date: delivery_date.getFullYear()+"-"+(((delivery_date.getMonth() < 9) ? '0': '') + (delivery_date.getMonth()+1))+"-"+delivery_date.getDay(),
                                order_id: orderId
                            },
                            id: jsonRpc.id
                        };
                        ch.sendToQueue(msg.properties.replyTo.toString(),Buffer.from(JSON.stringify(jsonRpcResponse)),{correlationId: msg.properties.correlationId,userId:userId});
                        updateStock(orderObject.yarns);
                    } 
                });
            }   
            else{
                let jsonRpcResponse={
                    jsonrpc: "2.0",
                    error: {
                             code: -32602,
                             message: "Invalid params",
                             data: "Not available in this moment"
                          },
                    id: jsonRpc.id
                };
                ch.sendToQueue(msg.properties.replyTo.toString(),Buffer.from(JSON.stringify(jsonRpcResponse)),{correlationId: msg.properties.correlationId,userId:userId});
            }
        });
    });
}
function confirmation(orderId,ch,jsonRpc,msg){
    processingConfirmationOrder(orderId,ch,jsonRpc,msg);
}